package com.thecrackertechnology.andrax

import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import android.view.Gravity
import android.widget.Toast
import com.onesignal.OneSignal
import com.thecrackertechnology.dragonterminal.component.NeoInitializer
import com.thecrackertechnology.dragonterminal.frontend.config.NeoPreference
import com.thecrackertechnology.dragonterminal.ui.bonus.BonusActivity
import com.thecrackertechnology.dragonterminal.utils.AssetsUtils
import org.acra.ACRA
import org.acra.BuildConfig
import org.acra.annotation.AcraCore
import org.acra.annotation.AcraDialog
import org.acra.annotation.AcraMailSender
import java.io.*
import java.net.MalformedURLException
import java.net.URL
import java.util.*

var versiondefault = "5005"

@AcraCore(buildConfigClass = BuildConfig::class)
@AcraMailSender(mailTo = "weidsom@thecrackertechnology.com")
@AcraDialog(resText = R.string.acra_dialog_text, resCommentPrompt = R.string.acra_dialog_comment, resTheme = R.style.AppCompatCrashDialog, resIcon = R.drawable.andraxicon, resTitle = R.string.acra_dialod_title)

class AndraxApp : Application() {

    override fun onCreate() {
        super.onCreate()

        isRooted(this)

        var result = false
        var stdin: OutputStream? = null
        val stdout: InputStream
        val params = ArrayList<String>()

        try {
            val pb = ProcessBuilder("su")
            pb.directory(File(this.applicationInfo.dataDir))
            pb.redirectErrorStream(true)
            val process: Process = pb.start()
            stdin = process.outputStream
            stdout = process.inputStream
            params.add(0, "PATH=" + this.filesDir.absolutePath + "/bin:\$PATH")
            params.add("exit 0")
            var os: DataOutputStream? = null

            try {
                os = DataOutputStream(stdin)
                for (cmd in params) {
                    os.writeBytes(cmd + "\n")
                }
                os.flush()
            } catch (e: IOException) {
                e.printStackTrace()
            } finally {
                os?.close()
            }



            if (process.waitFor() == 0) result = true
        } catch (e: Exception) {
            result = false
            e.printStackTrace()
        } finally {
            stdin?.close()

            val remountdata = Runtime.getRuntime().exec("su -c " + this.filesDir.absolutePath + "/bin/busybox mount -o remount,exec,suid,dev,rw /data")
            remountdata.waitFor()

            val auth_battery_hack = Runtime.getRuntime().exec("su -c /system/bin/dumpsys deviceidle whitelist +com.thecrackertechnology.andrax")
            auth_battery_hack.waitFor()

            app = this

            NeoPreference.init(this)
            //CrashHandler.init()
            NeoInitializer.init(this)

            OneSignal.startInit(this).inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification).unsubscribeWhenNotificationsAreDisabled(false).init()

            val sharedPref = this.getSharedPreferences(this.packageName, Context.MODE_PRIVATE)
            val issafepts = sharedPref.getBoolean("issafepts2", false)

            if(issafepts) {

                val AllDir = File(filesDir.absolutePath+"/scripts")
                AllDir.mkdirs()
                val BinDir = File(filesDir.absolutePath+"/bin")
                BinDir.mkdirs()

                AssetsUtils.extractAssetsDir(this, "all/scripts", this.filesDir.absolutePath+"/scripts")
                setPermissions(AllDir)

                AssetsUtils.extractAssetsDir(this, "arm/static/bin", this.filesDir.absolutePath+"/bin")
                setPermissions(BinDir)

            } else {

                val rmoldptslack = Runtime.getRuntime().exec("su -c rm -rf " + this.filesDir.absolutePath + "/bin")
                rmoldptslack.waitFor()

                val rmoldptslackscript = Runtime.getRuntime().exec("su -c rm -rf " + this.filesDir.absolutePath + "/scripts")
                rmoldptslackscript.waitFor()

                val AllDir = File(filesDir.absolutePath+"/scripts")
                AllDir.mkdirs()
                val BinDir = File(filesDir.absolutePath+"/bin")
                BinDir.mkdirs()

                AssetsUtils.extractAssetsDir(this, "all/scripts", this.filesDir.absolutePath+"/scripts")
                setPermissions(AllDir)

                AssetsUtils.extractAssetsDir(this, "arm/static/bin", this.filesDir.absolutePath+"/bin")
                setPermissions(BinDir)

                with (sharedPref.edit()) {
                    putBoolean("issafepts2", true)
                    commit()
                }

            }

            CheckVersion().execute(getString(R.string.andrax_version_link))

        }


    }

        override fun attachBaseContext(base: Context) {

            super.attachBaseContext(base)

            ACRA.init(this)
    }

    fun errorDialog(context: Context, message: Int, dismissCallback: (() -> Unit)?) {
        errorDialog(context, getString(message), dismissCallback)
    }

    fun errorDialog(context: Context, message: String, dismissCallback: (() -> Unit)?) {
        AlertDialog.Builder(context)
                .setTitle(R.string.error)
                .setMessage(message)
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(R.string.show_help, { _, _ ->
                    openHelpLink()
                })
                .setOnDismissListener {
                    dismissCallback?.invoke()
                }
                .show()
    }

    fun openHelpLink() {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://neoterm.gitbooks.io/neoterm-wiki/content/"))
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

    fun easterEgg(context: Context, message: String) {
        val happyCount = NeoPreference.loadInt(NeoPreference.KEY_HAPPY_EGG, 0) + 1
        NeoPreference.store(NeoPreference.KEY_HAPPY_EGG, happyCount)

        val trigger = NeoPreference.VALUE_HAPPY_EGG_TRIGGER

        if (happyCount == trigger / 2) {
            @SuppressLint("ShowToast")
            val toast = Toast.makeText(this, message, Toast.LENGTH_LONG)
            toast.setGravity(Gravity.CENTER, 0, 0)
            toast.show()
        } else if (happyCount > trigger) {
            NeoPreference.store(NeoPreference.KEY_HAPPY_EGG, 0)
            context.startActivity(Intent(context, BonusActivity::class.java))
        }
    }

    companion object {

        private var app: AndraxApp? = null

        fun get(): AndraxApp {
            return app!!
        }
    }


    class CheckVersion : AsyncTask<String, String, String>() {

        var VersionFromServer = 0

        override fun doInBackground(vararg fileUrl: String): String? {

            try {
                val url = URL(fileUrl[0])
                val urlConnection = url.openConnection()
                urlConnection.connectTimeout = 1000
                urlConnection.readTimeout = 1000
                urlConnection.setRequestProperty("User-Agent", "Mozilla/5.0 (Android ANDRAX; Mobile; rv:03) Gecko/67.0 Firefox/67.0")
                urlConnection.connect()

                val inputStream = urlConnection.getInputStream()
                val inputStreamReader = InputStreamReader(inputStream)
                val bufferedReader = BufferedReader(inputStreamReader)

                var resulversion = ""

                resulversion = bufferedReader.readLine()

                VersionFromServer = 0

                try {

                    VersionFromServer = Integer.parseInt(resulversion.replace("\\s+".toRegex(), ""))

                } catch (e: NumberFormatException) {

                }

            } catch (e: MalformedURLException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            }

            return null
        }

        override fun onProgressUpdate(vararg progress: String) {


        }

        override fun onPostExecute(result: String?) {

            if (VersionFromServer > Integer.parseInt(versiondefault)) {

                val notificationId = 9988
                val CHANNEL_ID = "NEWVERSION"
                val name = "New Version of ANDRAX"
                val importance = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    NotificationManager.IMPORTANCE_HIGH
                } else {
                    NotificationManagerCompat.IMPORTANCE_HIGH
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    NotificationChannel(CHANNEL_ID, name, importance)
                }

                val notificationIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://andrax.thecrackertechnology.com"))
                notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                val pendingIntent: PendingIntent = PendingIntent.getActivity(AndraxApp.get(), 0, notificationIntent, 0)

                val builder = NotificationCompat.Builder(AndraxApp.get(), CHANNEL_ID)
                        .setSmallIcon(R.drawable.andraxicon_svg)
                        .setChannelId(CHANNEL_ID)
                        .setContentTitle("NEW VERSION")
                        .setContentText("ANDRAX has a new version, DOWNLOAD NOW!")
                        .setStyle(NotificationCompat.BigTextStyle()
                                .bigText("ANDRAX has a new version, DOWNLOAD NOW!\nFor new tools, bug fixes and a lot of improvements"))
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setAutoCancel(false)
                        .setVibrate(longArrayOf(1000, 1000, 1000))
                        .setOngoing(true)
                        .setContentIntent(pendingIntent)
                        .setColor(0xFF000000.toInt())


                val notificationManager = NotificationManagerCompat.from(AndraxApp.get())

                notificationManager.notify(notificationId, builder.build())


            } else {

                val notificationManager = NotificationManagerCompat.from(AndraxApp.get())
                notificationManager.cancel(9988)

            }

        }
    }

    fun setPermissions(path: File?) {
        if (path == null) return
        if (path.exists()) {
            path.setReadable(true, false)
            path.setExecutable(true, false)
            val list = path.listFiles() ?: return
            for (f in list) {
                if (f.isDirectory) setPermissions(f)
                f.setReadable(true, false)
                f.setExecutable(true, false)
            }
        }

        var reader: BufferedReader? = null
        var testmsg=""
        var result = false
        var stdin: OutputStream? = null
        val stdout: InputStream
        val params = ArrayList<String>()

        try {
            val pb = ProcessBuilder("su")
            pb.directory(File(this.applicationInfo.dataDir))
            pb.redirectErrorStream(false)
            val process: Process = pb.start()
            stdin = process.outputStream
            stdout = process.inputStream
            params.add(0, "chmod -R 777 " + this.filesDir.absolutePath)
            params.add(1, "rm -rf" + this.filesDir.absolutePath + "/bin/su")
            params.add("exit 0")

            var os: DataOutputStream? = null

            try {
                os = DataOutputStream(stdin)
                for (cmd in params) {
                    os.writeBytes(cmd + "\n")
                }
                os.flush()
            } catch (e: IOException) {

                //e.printStackTrace()

            } finally {
                os?.close()
            }

            reader = BufferedReader(InputStreamReader(stdout))
            var n: Int
            val buffer = CharArray(1024)
            while (reader.read(buffer).also { n = it } != -1) {
                val msg = String(buffer, 0, n)

                testmsg += msg

            }

            if (process.waitFor() == 0) result = true
        } catch (e: Exception) {
            result = false
            //e.printStackTrace()
        } finally {
            reader?.close()
            stdin?.close()


        }


    }


    fun isRooted(c:Context):Boolean {
        var result = false
        var stdin: OutputStream? = null
        var stdout: InputStream? = null

        try {

            val process = Runtime.getRuntime().exec("su")
            stdin = process.getOutputStream()
            stdout = process.getInputStream()
            var os: DataOutputStream? = null

            try {
                os = DataOutputStream(stdin)
                os.writeBytes("ls /data\n")
                os.writeBytes("exit\n")
                os.flush()
            }

            catch (e:IOException) {
                e.printStackTrace()
            }

            finally {
                os?.close()
            }

            var n = 0
            var reader: BufferedReader? = null

            try {
                reader = BufferedReader(InputStreamReader(stdout))
                while (reader.readLine() != null) {
                    n++
                }
            }

            catch (e:IOException) {
                e.printStackTrace()
            }

            finally {
                reader?.close()
            }

            if (n > 0) {
                result = true
            }
        }
        catch (e:IOException) {
            e.printStackTrace()
        }

        finally {
            stdout?.close()
            stdin?.close()
        }

        if (!result) {
            val intent = Intent(this, RootIt::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }

        return result
    }


    fun checkcoreversion() {

        var reader: BufferedReader? = null
        var testmsg=""
        var result = false
        var stdin: OutputStream? = null
        val stdout: InputStream
        val params = ArrayList<String>()

        try {
            val pb = ProcessBuilder("su")
            pb.directory(File(this.applicationInfo.dataDir))
            pb.redirectErrorStream(false)
            val process: Process = pb.start()
            stdin = process.outputStream
            stdout = process.inputStream
            params.add(0, "cat " + this.applicationInfo.dataDir + "/tmpsystem/version")
            params.add("exit 0")

            var os: DataOutputStream? = null

            try {
                os = DataOutputStream(stdin)
                for (cmd in params) {
                    os.writeBytes(cmd + "\n")
                }
                os.flush()
            } catch (e: IOException) {

                //e.printStackTrace()

            } finally {
                os?.close()
            }

            reader = BufferedReader(InputStreamReader(stdout))
            var n: Int
            val buffer = CharArray(1024)
            while (reader.read(buffer).also { n = it } != -1) {
                val msg = String(buffer, 0, n)

                testmsg += msg

            }

            if (process.waitFor() == 0) result = true
        } catch (e: Exception) {
            result = false
            e.printStackTrace()
        } finally {
            reader?.close()
            stdin?.close()

            if(Integer.parseInt(testmsg.replace("\\s+".toRegex(), "")) != Integer.parseInt(versiondefault)) {

                val intent = Intent(this, CheckCoreVersion::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)

            }

        }

    }


}
